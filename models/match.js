var mongoose = require('mongoose');

var Match = mongoose.model('Match', {
    team01: {
        type: String,
        required: true,
        trim: true
    },
    scoreTeam01: {
        type: integer,
        default: 0
    },
    team02: {
        type: String,
        required: true,
        trim: true
    },
    scoreTeam02: {
        type:integer,
        default:0
    }

});

module.exports = {Match};