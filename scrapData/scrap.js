const cheerio = require('cheerio');
const request = require('request');
const moment = require('moment');
moment.locale('');

var count = 0;
var arrayDate = [];
var arrayMatch = [];
var arrayCompetition = [];
var matches = [];
var finalJSON = {};

const cleaningMatchData = (str) => {
    
    //cleaning data
    str = str.replace(/(\r\n\t|\n|\r\t)/gm,"");
    str = str.replace('         ',' ');
    return (str);
    
}

const compileSummaryJson = (match, date, competition, matchNumber) => {

    const matches = [];
    

    for (let index = 0; index < match.length; index++) {
    
        //separating string into teams and scores

        firstHalf = match[index].substring(0, match[index].indexOf('x'));
        const teamHome = firstHalf.trimRight().substring(0, firstHalf.indexOf(' '));
        scoreTeamHome = firstHalf.substring(firstHalf.indexOf(' ')).trimLeft();
        secondHalf = match[index].replace(firstHalf,'').trimLeft();
        teamAway = secondHalf.substring(secondHalf.indexOf(' ')).trimLeft();
        scoreTeamAway = secondHalf.substring(1,secondHalf.indexOf(' '));
        
        //format date yyyy-mm-dd using moment
        
        day = date[index].split('-')[0];
        month = date[index].split('-')[1];
        year = date[index].split('-')[2];
        matchDate = moment(year+"-"+month+"-"+day).format('YYYY-MM-DD');


        const summary = {
            team_home: teamHome,
            team_away: teamAway, 
            score_team_home: scoreTeamHome, 
            score_team_away: scoreTeamAway, 
            date: matchDate, 
            competition,
        };
        matches.push({"number": matchNumber, summary, "details":{}})
        
    }
    //console.log(matches.length);
    // console.log(JSON.stringify(matches,undefined,4));
    return matches;

}

const scrapData = (url,years) => {
 
    // console.log('url: ', url);
    request(url, function(error, reponse, html) {
        
        console.log(years);
        var $ = cheerio.load(html);
        var match
        if(!error) {
            
            $('table').each(function(tbody, elem) {
                
                var matchOK = 0;
                var dateOK = 0;
                var competitionOK = 0;
    
                var match = $(this).find('font').children().find('i').html();
    
                if(match !== undefined) {
                    if(match != null) {
                        matchOK = 1;
                        match = cleaningMatchData(match);
                        arrayMatch.push(match);
                    }
                }
    
                var date = $(this).find('tr').find('td').find('font').text();
                date = date.split('(Date): ',+5)[1];
    
                if((date !== undefined) && (date != null))  {
                    date = date.substring(0,10).trimRight();
                    arrayDate.push(date);
                }
    
                var competition = $(this).find('tr').find('td').find('font').text();
                competition = competition.split('(Competition): ',+18)[1];
                if(competition !== undefined) {
                    if(competition != null) {
                        competition = competition.split(' Local')[0].trimRight();
                        if(competition.indexOf("(")) {
                            competition = competition.substring(competition.indexOf('(') + 1, competition.indexOf(')'));
                        }
                        arrayCompetition.push(competition);
                    }
                } 
                    
            });
        }

        console.log(compileSummaryJson(arrayMatch, arrayDate, arrayCompetition));
    
        return compileSummaryJson(arrayMatch, arrayDate, arrayCompetition);
 
    });
}

var initScrap = () => {
    
    var periods = ['191422','192332','193438','193946','194752',
    '195356','195758','195960','196163','196466','196768','196970','197173',
    '197475','197677','197879','198081','198284','198587','198889','199091',
    '199293','199495','199697','199899','200001','200203','200405','200607',
    '200809','201011','201213','201415','201617','2018'];

    const scout = [];
    
    for (let index = 0; index < periods.length; index++) {
        url = `https://www.rsssfbrasil.com/sel/brazil${periods[index]}.htm`;
        
        let years = periods[index];
        
        if(periods[index].length > 4) {
            years = periods[index].substring(0,4)+"-"+periods[index].substring(4,6);
        }

        // console.log(years);
        
        const matches = scrapData(url,years);
        scout.push({
            matches,
            years
        });
        console.log(scout);
        
    }
}

//console.log(initScrap());
initScrap();
module.exports = initScrap;
