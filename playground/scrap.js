const cheerio = require('cheerio');
const request = require('request');

var periods = ['190613','191422','192332','193438','193946','194752',
'195356','195758','195960','196163','196466','196768','196970','197173',
'197475','197677','197879','198081','198284','198587','198889','199091',
'199293','199495','199697','199899','200001','200203','200405','200607',
'200809','201011','201213','201415','201617','2018'];

var period = Math.trunc(Math.random() * (periods.length - 0) + 0);

var arrayDate = [];
var arrayMatch = [];
var arrayCompetition = [];
var count = 1;
var matches = {};


const cleaningMatchData = (str) => {
    
    //cleaning data
    str = str.replace(/(\r\n\t|\n|\r\t)/gm,"");
    str = str.replace('         ',' ');
    return (str);
    
}

const compileMatchJson = (match, date, competition) => {
    
    for (let index = 0; index < match.length; index++) {
        
        Details = {};
    
        //separating string into teams and scores

        firstHalf = match[index].substring(0, match[index].indexOf('x'));
        team01 = firstHalf.trimRight().substring(0, firstHalf.indexOf(' '));
        scoreTeam01 = firstHalf.substring(firstHalf.indexOf(' ')).trimLeft();
        secondHalf = match[index].replace(firstHalf,'').trimLeft();
        team02 = secondHalf.substring(secondHalf.indexOf(' ')).trimLeft();
        scoreTeam02 = secondHalf.substring(1,secondHalf.indexOf(' '));
        
        Details.team01 = team01;
        Details.team02 = team02;
        Details.scoreTeam01 = scoreTeam01;
        Details.scoreTeam02 = scoreTeam02;
        Details.date = date[index];
        Details.competition = competition[index];
        matches[count] = Details;
        count++;
    }
    console.log(JSON.stringify(matches,undefined,4));
    return matches;
}


var scrapData = (url) => {
    
    // console.log('url: ', url);
    request(url, function(error, reponse, html) {
    
        var $ = cheerio.load(html);
    
        if(!error) {
            
            $('table').each(function(tbody, elem) {
                
                var matchOK = 0;
                var dateOK = 0;
                var competitionOK = 0;
    
                var match = $(this).find('font').children().find('i').html();
    
                if(match !== undefined) {
                    if(match != null) {
                        matchOK = 1;
                        match = cleaningMatchData(match);
                        arrayMatch.push(match);
                    }
                }
    
                var date = $(this).find('tr').find('td').find('font').text();
                date = date.split('(Date): ',+5)[1];
    
                if((date !== undefined) && (date != null))  {
                    date = date.substring(0,10).trimRight();
                    arrayDate.push(date);
                }
    
                var competition = $(this).find('tr').find('td').find('font').text();
                competition = competition.split('(Competition): ',+18)[1];
                if(competition !== undefined) {
                    if(competition != null) {
                        competition = competition.split(' Local')[0].trimRight();
                        if(competition.indexOf("(")) {
                            competition = competition.substring(competition.indexOf('(') + 1, competition.indexOf(')'));
                        }
                        arrayCompetition.push(competition);
                    }
                } 
                    
            });
        }
    
        compileMatchJson(arrayMatch, arrayDate, arrayCompetition);

    });

}

for (let index = 0; index < periods.length; index++) {
    url = `https://www.rsssfbrasil.com/sel/brazil${periods[index]}.htm`;
    scrapData(url);
    
}


scrapData(url);