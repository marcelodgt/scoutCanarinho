var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app = express();
var {mongoose} = require('./db/mongoose.js');
var {}

app.get('/scrape', function(req, res){

    //scrapping
    url = 'https://www.rsssfbrasil.com/sel/brazil2018.htm';
    
    var periods = ['1906-1913','1914-1922','1923-1932','1934-1938','1939-1946','1947-1952',
    '1953-1956','1957-1958','1959-1960','1961-1963','1964-1966','1967-1968','1969-1970','1971-1973',
    '1974-1975','1976-1977','1978-1979','1980-1981','1982-1984','1985-1987','1988-1989','1990-1991',
    '1992-1993','1994-1995','1996-1997','1998-1999','2000-2001','2002-2003','2004-2005','2006-2007',
    '2008-2009','2010-2011','2012-2013','2014-2015','2016-2017','2018'];

    
    
    request(url, function(error, response, html) {
        if(!error){
            
            var $ = cheerio.load(html);
            
            //initialize match json
            const matches = {};
            

            $('table').each(function(tr, elem) {
                
                //get match score from the html
                var match =  $(this).find('font').children().find('i').html();
                //initializing individual json
                var matchJson = {team01:"", team02:"", scoreteam01:"", scoreteam02:""};
                if(match != null) {
                    if(match.length > 0) {

                        //cleaning data
                        match = match.replace(/(\r\n\t|\n|\r\t)/gm,"");
                        match = match.replace('         ',' ');
                        
                        //separating string into team0s ans scores
                        team01 = match.split('x')[0].split(' ')[0];
                        scoreteam01 = match.split('x')[0].split(' ')[1];
                        team02 = match.split('x')[1].split(' ')[1];
                        scoreteam02 = match.split('x')[1].split(' ')[0];
                        
                        //saving in an individual json
                        matchJson.team01 = team01;
                        matchJson.team02 = team02;
                        matchJson.scoreteam01 = scoreteam01;
                        matchJson.scoreteam02 = scoreteam02;
                        
                        //send to general json
                        matches[tr] = matchJson;
                    }
                }
            });  
        }
    })
})

app.listen('8081');

console.log('Server started on port 8081');

exports = module.exports = app;
